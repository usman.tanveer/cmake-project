#include <string>
#include <iostream>
#include "student.h"
using namespace std;

int const class_size = 2;
int flag, student_count=0, temp_int;
string temp_str;
float temp_float;
char temp_char;
bool temp_bool=false;

void add_student(student[]);
void remove_student(student[]);
void print_class(student[]);
void goto_results(student[]);
void get_marks(student[], int);
void enter_marks(student[], int);
void show_marks(student[], int);

int main(){
    student st[class_size];

    while(true){
        cout << "Select function: \n1 -> Add student\n2 -> Remove student\n3 -> Print class\n4 -> Results\nAny other number -> Exit\n";
        cin >> flag;
        switch (flag)
        {
        case 1:
            add_student(st);
            break;
        case 2:
            remove_student(st);
            break;
        case 3:
            print_class(st);
            break;
        case 4:
            goto_results(st);
            break;
        default:
            return 0;
        }
    }
    return 0;
}

void add_student(student st[]){
    if(student_count < class_size){
        cout << "Enter Roll No. ";
        cin >> temp_str;
        st[student_count].set_roll_no(temp_str);
        cout << "Enter Age ";
        cin >> temp_int;
        if(temp_int < 10 || temp_int > 60){
            st[student_count].set_roll_no("");
            cout << "Invalid Age, try again...\n\n";
            return;
        }
        st[student_count].set_age(temp_int);
        cout << "Enter CGPA ";
        cin >> temp_float;
        if(temp_float < 0 || temp_float > 4){
            st[student_count].set_roll_no("");
            st[student_count].set_age(0);
            cout << "Invalid CGPA, try again...\n\n";
            return;
        }
        st[student_count].set_cgpa(temp_float);
        student_count++;
    }
    else{
        cout << "Cannot add, Class is full\n\n";
    }
}

void remove_student(student st[]){
    if(student_count > 0){
            cout << "Enter the roll no. ";
            cin >> temp_str;
            for(int i = 0; i < student_count; i++){
                if(st[i].get_roll_no() == temp_str){
                    st[i].set_age(st[student_count-1].get_age()); st[student_count-1].set_age(0);
                    st[i].set_cgpa(st[student_count-1].get_cgpa()); st[student_count-1].set_cgpa(0);
                    st[i].set_roll_no(st[student_count-1].get_roll_no()); st[student_count-1].set_roll_no("");
                    student_count--;
                }
            }
        }
        else{
            cout << "Cannot remove, Class is empty\n\n";
        }
}

void print_class(student st[]){
    for(int i=0; i<student_count; i++){
        cout << "\nRoll No.: " << st[i].get_roll_no() <<
        "\nAge: " << st[i].get_age() << "\nCGPA: " << st[i].get_cgpa() << "\n\n";
    }
}

void goto_results(student st[]){
    bool out = false;
    cout << "Enter student Roll No. ";
        cin >> temp_str;
        for(int i=0; i<student_count; i++){
            if(st[i].get_roll_no() == temp_str){
                temp_bool = true;
                while(true){
                    cout << "Select Function:\n1 -> Get marks\n2 -> Enter marks\n3 -> Show marks\nAny other number -> Previous option\n";
                    cin >> temp_int;
                    if(isdigit(temp_int)){
                        cout << "Enter a number\n\n";
                        break;
                    }
                    else{
                        switch (temp_int)
                        {
                        case 1:
                            get_marks(st, i);
                            break;
                        case 2:
                            enter_marks(st, i);
                            break;
                        case 3:
                            show_marks(st, i);
                            break;
                        default:
                            out = true;
                            break;
                        }
                    }
                    if(out == true){
                        out = false;
                        break;
                    }
                }
            }
        }
        if(temp_bool == false){
            cout << "Student Not Found\n\n";
        }
        else{
            temp_bool = false;
        }
}

void get_marks(student st[], int i){
    cout << "Enter Subject ";
    cin >> temp_str;
    if(st[i].get_subject_marks(temp_str) != -1)
        cout << "Marks: " << st[i].get_subject_marks(temp_str) << "\n\n";
}

void enter_marks(student st[], int i){
    cout << "Enter Subject ";
    cin >> temp_str;
    cout << "Enter Marks ";
    cin >> temp_int;
    if(temp_int < 0 || temp_int > 100){
        cout << "Invalid Marks, try again...\n\n";
        return;
    }
    st[i].set_subject_marks(temp_str, temp_int);
}

void show_marks(student st[], int i){
    st[i].print_all_markds();
}