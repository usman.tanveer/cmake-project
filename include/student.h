#include <map>
#include <iostream>
#include <string>
class student{
private:
    struct student_record{
        std::string roll_no;
        int age;
        float cgpa;
    }record;
    std::map<std::string, int> result;

public:
    student();
    ~student();
    int get_subject_marks(std::string subject);
    void set_subject_marks(std::string subject, int marks);
    void print_all_markds();

    std::string get_roll_no();
    int get_age();
    float get_cgpa();
    void set_roll_no(std::string roll_no);
    void set_age(int age);
    void set_cgpa(float cgpa);
};