#include <student.h>

student::student(){
    record.age = 0;
    record.cgpa = 0;
    record.roll_no = "";
    std::cout << "Constructor called\n";
}
student::~student(){
    std::cout << "Destructor called\n";
}
int student::get_subject_marks(std::string subject){
    if(result.find(subject) != result.end()){
        return result[subject];
    }
    else{
        std::cout << "Subject Not Found...!\n\n";
        return -1;
    }
}
void student::set_subject_marks(std::string subject, int marks){
    result[subject] = marks;
}
void student::print_all_markds(){
    std::map<std::string, int> :: iterator iter;
    for(iter=result.begin(); iter!=result.end(); iter++){
        std::cout << "Subject: " << (*iter).first << "\nMarks: " << (*iter).second << "\n\n";
    }
}
std::string student::get_roll_no(){return record.roll_no;}
int student::get_age(){return record.age;}
float student::get_cgpa(){return record.cgpa;}
void student::set_roll_no(std::string roll_no){record.roll_no = roll_no;}
void student::set_age(int age){record.age = age;}
void student::set_cgpa(float cgpa){record.cgpa = cgpa;}
